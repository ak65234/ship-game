window.addEventListener('DOMContentLoaded', function () {
    const cvs = document.querySelector("canvas");
    const ctx = cvs.getContext("2d");

    let paused = false;
    let pausing = false;
    let flicker = false;

    const ship = {
        x: 0,
        y: 0,
        z: 10,
        vel: {
            x: 0,
            y: 0
        },
        speed: 5,
        acc: 3,
        moving: false,
        shot: 0
    }

    const stars = [];
    const starScalar = 3;

    const bullets = [];
    const bulletSpeed = 10;

    const enemies = [];

    const keys = {};

    function shoot() {
        bullets.push({
            x: ship.x + ship.z * 3,
            y: ship.y + ship.z * 5 / 2 - ship.z * 2 / 4,
            z: ship.z / 4
        });
    }

    function control() {
        let isMoving = paused ? ship.moving : false;

        for (const code in keys) {
            switch (code) {
                case 'ArrowLeft':
                    if (keys[code] && !paused) {
                        ship.vel.x -= ship.acc;
                        isMoving = true;
                    }
                    break;
                case 'ArrowRight':
                    if (keys[code] && !paused) {
                        ship.vel.x += ship.acc;
                        isMoving = true;
                    }
                    break;
                case 'ArrowUp':
                    if (keys[code] && !paused) {
                        ship.vel.y -= ship.acc;
                        isMoving = true;
                    }
                    break;
                case 'ArrowDown':
                    if (keys[code] && !paused) {
                        ship.vel.y += ship.acc;
                        isMoving = true;
                    }
                    break;
                case 'Space':
                    if (!paused && keys[code] && !ship.shot) {
                        ship.shot = 7;
                        shoot();
                    }
                    break;
                case 'KeyP':
                    if (keys[code]) {
                        pausing = true;
                    } else if (pausing === true) {
                        paused = !paused;
                        pausing = false;
                    }
                    break;
            }
        }

        const mag = Math.sqrt(Math.pow(ship.vel.x, 2) + Math.pow(ship.vel.y, 2));
        if (mag > ship.speed) {
            ship.vel.x = ship.speed * ship.vel.x / mag;
            ship.vel.y = ship.speed * ship.vel.y / mag;
        }

        if (!paused) {
            ship.x += ship.vel.x;
            ship.y += ship.vel.y;
        }

        const drag = 10;

        if (ship.vel.x < 0) {
            ship.vel.x += ship.acc / drag;
        } else {
            ship.vel.x -= ship.acc / drag;
        }

        if (ship.vel.y < 0) {
            ship.vel.y += ship.acc / drag;
        } else {
            ship.vel.y -= ship.acc / drag;
        }

        if (ship.x < ship.z / 2) {
            ship.x = ship.z / 2;
        } else if (ship.x > cvs.width - ship.z * 3) {
            ship.x = cvs.width - ship.z * 3;
        }

        if (ship.y < 0) {
            ship.y = 0;
        } else if (ship.y > cvs.height - ship.z * 5) {
            ship.y = cvs.height - ship.z * 5;
        }

        if (ship.shot > 0) {
            ship.shot--;
        }

        ship.moving = isMoving;
    }

    function drawShip() {
        ctx.fillStyle = '#0c0';
        ctx.fillRect(ship.x, ship.y, ship.z, ship.z * 5);
        ctx.fillRect(ship.x + ship.z / 2, ship.y + ship.z, ship.z * 3 / 2, ship.z * 3);
        ctx.fillRect(ship.x + ship.z / 2, ship.y + ship.z * 2, ship.z * 5 / 2, ship.z);
    }

    function drawBurners() {
        ctx.fillStyle = flicker ? "#f90" : "#ff0";

        if (ship.moving) {
            ctx.fillRect(ship.x - ship.z / 2, ship.y + ship.z, ship.z, ship.z);
            ctx.fillRect(ship.x - ship.z / 2, ship.y + ship.z * 3, ship.z, ship.z);
        }

        for (const enemy of enemies) {
            ctx.fillRect(enemy.x + enemy.z * 5 / 2, enemy.y + enemy.z, enemy.z, enemy.z);
            ctx.fillRect(enemy.x + enemy.z * 5 / 2, enemy.y + enemy.z * 3, enemy.z, enemy.z);
        }
    }

    function moveBullets() {
        if (paused) {
            return;
        }

        for (const bullet of bullets) {
            bullet.x += bulletSpeed;

            if (bullet.x >= cvs.width + bullet.z * 4) {
                bullets.splice(bullets.indexOf(bullet), 1);
            }
        }
    }

    function drawBullets() {
        for (const bullet of bullets) {
            ctx.fillStyle = flicker ? "#f00" : "#ff0";
            ctx.fillRect(bullet.x, bullet.y + bullet.z, bullet.z * 4, bullet.z * 2);
            ctx.fillRect(bullet.x + bullet.z, bullet.y, bullet.z * 2, bullet.z * 4);

            ctx.fillStyle = flicker ? "#ff0" : "#f00";
            ctx.fillRect(bullet.x + bullet.z, bullet.y + bullet.z, bullet.z * 2, bullet.z * 2);
        }
    }

    function makeStar() {
        return {
            x: Math.random() * cvs.width,
            y: Math.random() * cvs.height,
            z: Math.random() * 2 + 1
        };
    }

    function generateStars(amount) {
        for (let i = 0; i < amount; i++) {
            stars.push(makeStar());
        }
    }

    function moveStars() {
        if (paused) {
            return;
        }

        for (const star of stars) {
            star.x -= star.z * starScalar;
            if (star.x < -3) {
                star.x = cvs.width + 3;
                star.y = Math.random() * cvs.height;
                star.z = Math.random() * 2 + 1;
            }
        }
    }

    function drawStars() {
        ctx.fillStyle = "#fff";
        for (const star of stars) {
            ctx.fillRect(star.x, star.y, star.z, star.z);
        }
    }

    function makeEnemy() {
        enemies.push({
            x: cvs.width + Math.random() * cvs.width / 2,
            y: Math.random() * (cvs.height - ship.z * 5),
            z: ship.z,
            speed: 3
        });
    }

    function moveEnemies() {
        if (paused) {
            return;
        }

        for (const enemy of enemies) {
            enemy.x -= enemy.speed;

            if (enemy.x + enemy.z * 7 / 2 < 0) {
                enemy.x += cvs.width + enemy.z * 7 / 2 + Math.random() * cvs.width / 2;
                enemy.y = Math.random() * (cvs.height - ship.z * 5);
            }
        }
    }

    function drawEnemies() {
        ctx.fillStyle = "#900";
        for (const enemy of enemies) {
            ctx.fillRect(enemy.x, enemy.y + enemy.z * 2, enemy.z * 5 / 2, enemy.z);
            ctx.fillRect(enemy.x + enemy.z, enemy.y + enemy.z, enemy.z * 3 / 2, enemy.z * 3);
            ctx.fillRect(enemy.x + enemy.z * 2, enemy.y, enemy.z, enemy.z * 5);
        }
    }

    function collide() {
        for (const enemy of enemies) {
            for (const bullet of bullets) {
                if (bullet.x + bullet.z * 4 >= enemy.x
                    && enemy.x + enemy.z * 3 >= bullet.x
                    && bullet.y + bullet.z * 4 >= enemy.y
                    && enemy.y + enemy.z * 5 >= bullet.y) {
                    bullet.x = cvs.width * 2;
                    enemy.x = -enemy.z * 7 / 2;
                    break;
                }
            }

            if (ship.x + ship.z * 3 >= enemy.x
                && enemy.x + enemy.z * 3 >= ship.x
                && ship.y + ship.z * 5 >= enemy.y
                && enemy.y + enemy.z * 5 >= ship.y) {
                ship.x = ship.z / 2;
                ship.y = 0;
                enemy.x = -enemy.z * 7 / 2;
                break;
            }
        }
    }

    function draw() {
        ctx.fillStyle = "#000";
        ctx.fillRect(0, 0, cvs.width, cvs.height);

        flicker = Date.now() % 200 < 100;

        control();
        moveStars();
        moveBullets();
        moveEnemies();

        collide();

        drawStars();
        drawBullets();

        drawBurners();
        drawEnemies();
        drawShip();

        requestAnimationFrame(draw)
    }

    window.addEventListener('keydown', function (evt) {
        keys[evt.code] = true;
        // console.log(evt.code);
    }, false);

    window.addEventListener('keyup', function (evt) {
        keys[evt.code] = false;
    }, false);

    generateStars(300);
    for (let i = 0; i < 15; i++) {
        makeEnemy();
    }
    requestAnimationFrame(draw);
}, false);